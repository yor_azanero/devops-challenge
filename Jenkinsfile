import hudson.model.*
import hudson.EnvVars
import groovy.json.JsonSlurperClassic
import groovy.json.JsonBuilder
import groovy.json.JsonOutput
import java.net.URL
import java.net.URLEncoder

def DEPLOYMENT_OBJECT
@NonCPS

pipeline {

    agent {
        label getLabel()
    }

    environment {
        IMAGE_NAME = 'devops/challengue'
        AWS_REGION = 'us-east-1'
        AWS_ACCOUNT = '123548956464'
        AWS_CLI_VERSION = '1.16'
        IMAGE_TAG = getShortCommitId()
    }

    stages {
        stage('Initialize') {
            steps {
                echo "iniciando"
            }
        }
        stage('Unit Test') {
          steps {
              script {
                  docker.image('node:12.13').inside("-u 0") {
                      sh (returnStdout: false, script: "npm install", label: "Installing Dependencies")
                      sh (returnStdout: false, script: "npm test -- tests --runInBand", label: "Running Test ...")
                }
             }
          }
        }

        stage('Build image') {
            steps {
                script {
                    def repositoryName = "${AWS_ACCOUNT}.dkr.ecr.${AWS_REGION}.amazonaws.com/${IMAGE_NAME}"
                    def concatImagesTags = ""

                    def defaultTagsList = [
                      "${repositoryName}:${IMAGE_TAG}",
                      "${repositoryName}:${getFixedImageTag()}"
                    ]

                    defaultTagsList.each { imageTag ->
                       concatImagesTags = "${concatImagesTags}" + " -t ${imageTag}"
                    }

                    sh "\$(aws ecr get-login --no-include-email --region ${AWS_REGION})"
                    sh "docker build ${concatImagesTags} ."
                }
            }
        }
    }

    post {
        always {
            cleanWs()
        }
    }
}

def getFixedImageTag() {
    return (isDevelop())?'dev':(isRelease()?'qas':(isMaster())?'latest':'qas')
}

def isMaster() {
    return env.BRANCH_NAME == "master"
}

def isRelease() {
    return env.BRANCH_NAME ==~ '^release\\/[\\w\\d\\.]*$'
}

def isDevelop() {
    return env.BRANCH_NAME == "develop"
}

def getShortCommitId() {
    def gitCommit = env.GIT_COMMIT
    def shortGitCommit = "${gitCommit[0..6]}"
    return shortGitCommit
}

def getLabel(){
  def configuration = jenkins.model.Jenkins.instance.getItem(env.JOB_NAME.minus("/${env.JOB_BASE_NAME}"))
  return (configuration.getDescription() != '')?configuration.getDescription():'ec2-linux-spot-slave'
}