terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

locals {
  tags = {
    Name = "app_server"
  }
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

resource "aws_instance" "app_server" {
  ami = "ami-04902260ca3d33422"
  instance_type = "t3a.nano"
  capacity_reservation_specification {
    capacity_reservation_preference = "open"    
  }
  root_block_device {
    encrypted = false
    volume_type = "gp2"
    volume_size = 20
    tags = local.tags
  }
 
  tags = local.tags
}

resource "aws_security_group" "app_server" {
  name = "new-sg"
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["11.22.33.44/32"]
  }
  tags = local.tags
}